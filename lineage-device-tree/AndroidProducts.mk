#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_itel-S665L.mk

COMMON_LUNCH_CHOICES := \
    lineage_itel-S665L-user \
    lineage_itel-S665L-userdebug \
    lineage_itel-S665L-eng
